package com.pabcarr;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.Before;

/**
 * Pruebas unitarias de caja negra de la parte del constructor de la clase Asignatura
 * 
 * @author pabcarr
 */
public class AtributosAsignaturaTest {

	private final double ERROR_MAXIMO = 0.001; 
	private Asignatura asignatura;
	private Date fechaInicio;
	private Date fechaFin;
	
	@Before
	public void setUp(){
		asignatura = new Asignatura();
		fechaInicio = new Date(199);
		fechaFin = new Date(300);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNombreAsignaturaVacio(){
		asignatura.setNombre("");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNombreAsignaturaNull(){
		asignatura.setNombre(null);
	}
	
	@Test
	public void testNombreAsignaturaCorrecto(){
		asignatura.setNombre("TDS");
		assertEquals(asignatura.getNombre(), "TDS");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testDescripcionAsignaturaVacio(){
		asignatura.setDescripcion("");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testDescripcionAsignaturaNull(){
		asignatura.setDescripcion(null);
	}
	
	@Test
	public void testDescripcionAsignaturaCorrecto(){
		asignatura.setDescripcion("Asignatura muy bonita");
		assertEquals(asignatura.getDescripción(), "Asignatura muy bonita");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCalificacionMaximaCero(){
		asignatura.setCalificacionMaxima(0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCalificacionMaximaNegativa(){
		asignatura.setCalificacionMaxima(-0.5);
	}
	
	@Test
	public void testCalificacionMaximaPositiva(){
		asignatura.setCalificacionMaxima(1);
		assertEquals(asignatura.getCalificacionMaxima(), 1, ERROR_MAXIMO);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFechaInicioNull(){
		asignatura.setFechaInicio(null);
	}
	
	@Test
	public void testFechaInicioCorrecta(){
		asignatura.setFechaInicio(fechaInicio);
		assertEquals(asignatura.getFechaInicio(), fechaInicio);
	} 
	
	@Test(expected = IllegalArgumentException.class)
	public void testFechaFinNull(){
		asignatura.setFechaFin(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFechaFinAntesFechaInicio(){		
		asignatura.setFechaInicio(fechaFin);
		asignatura.setFechaFin(fechaInicio);
	}
	
	
	@Test
	public void testFechaFinDespuesFechaInicio(){
		asignatura.setFechaInicio(fechaInicio);
		asignatura.setFechaFin(fechaFin);
		
		assertEquals(asignatura.getFechaFin(), fechaFin);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFechaFinIgualFechaInicio(){
		asignatura.setFechaInicio(fechaInicio);
		asignatura.setFechaFin(fechaInicio);
	}
	
	@Test
	public void testCrearAsignaturaCompletaCorrecta() {
		asignatura.setNombre("TDS");
		asignatura.setDescripcion("muy bonita");
		asignatura.setCalificacionMaxima(10.0);
		asignatura.setFechaInicio(fechaInicio);
		asignatura.setFechaFin(fechaFin);
		
		assertEquals(asignatura.getNombre(), "TDS");
		assertEquals(asignatura.getDescripción(), "muy bonita");
		assertEquals(asignatura.getCalificacionMaxima(), 10.0, ERROR_MAXIMO);
		assertEquals(asignatura.getFechaInicio(), fechaInicio);
		assertEquals(asignatura.getFechaFin(), fechaFin);
	}
	
	@After
	public void tearDown(){
		asignatura = null;
		fechaInicio = null;
		fechaFin = null;
	}
	
}
