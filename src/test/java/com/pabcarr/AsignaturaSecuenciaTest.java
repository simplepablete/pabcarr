package com.pabcarr;

import java.util.Date;

import org.easymock.Mock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.Mock.*;
import static org.junit.Assert.*;

public class AsignaturaSecuenciaTest {

	private Date fecha1;
	private Date fecha2;
	private Date fecha3;
	private Date fecha4;
	private Date fecha5;
	@Mock
	private IPrueba prueba1;
	@Mock
	private IPrueba prueba2;
	@Mock
	private IPrueba prueba3;
	private Asignatura asignatura;
	
	@Before
	public void setUp() {
		
		prueba1 = createMock(IPrueba.class);
		prueba2 = createMock(IPrueba.class);
		prueba3 = createMock(IPrueba.class);
		
		fecha1 = new Date(200);
		fecha2 = new Date(300);
		fecha3 = new Date(400);
		fecha4 = new Date(500);
		fecha5 = new Date(600);
		asignatura = new Asignatura("TDS", "buena", 10.0, fecha1, fecha5);
		
		expect(prueba1.getFecha()).andReturn(fecha2).anyTimes();
		expect(prueba2.getFecha()).andReturn(fecha3).anyTimes();
		expect(prueba3.getFecha()).andReturn(fecha4).anyTimes();
		expect(prueba1.isCompletamenteCalificada()).andReturn(true).anyTimes();
		expect(prueba2.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba3.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba1.getPeso()).andReturn(0.3).anyTimes();
		expect(prueba2.getPeso()).andReturn(0.3).anyTimes();
		expect(prueba3.getPeso()).andReturn(0.4).anyTimes();
		replay(prueba1);
		replay(prueba2);
		replay(prueba3);	
	}
	
	@Test
	public void testSecuenciaAñadirAsignaturas() {

		
		asignatura.añadirPrueba(prueba1);
		asignatura.añadirPrueba(prueba2);
		asignatura.añadirPrueba(prueba3);
		
		assertTrue(asignatura.getPruebas().contains(prueba1));
		assertTrue(asignatura.getPruebas().contains(prueba1));
		assertTrue(asignatura.getPruebas().contains(prueba1));
		assertEquals(asignatura.getPruebas().size(), 3);
	}
	
	@After
	public void tearDown() {
		fecha1 = null;
		fecha2 = null;
		fecha3 = null;
		fecha4 = null;
		fecha5 = null;
		asignatura = null;
		
	}
}
