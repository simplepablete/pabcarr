package com.pabcarr;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.Date;

import org.easymock.Mock;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;

/**
 * @author pabcarr
 */
public class AsignaturaAislamientoTest {

	private Asignatura asignatura;
	private Date date1;
	private Date date2;
	private Date date3;
	
	
	@Mock
	private IPrueba prueba;
	@Mock
	private IPrueba prueba1;
	@Mock
	private IPrueba prueba2;
	@Mock
	private IPrueba prueba3;
	
	
	@Before
	public void setUp() {
		date1 = new Date(200);
		date2 = new Date(300);
		date3 = new Date(250);
		asignatura = new Asignatura("TDS", "bonita", 10.0, date1, date2);

		prueba = createMock(IPrueba.class);
		prueba1 = createMock(IPrueba.class);
		prueba2 = createMock(IPrueba.class);
		prueba3 = createMock(IPrueba.class);
	}
	
	@After
	public void tearDown() {
		asignatura = null;
		date1 = null;
		date2 = null;
		date3 = null;
		
		prueba = null;
		prueba1 = null;
		prueba2 = null;
		prueba3 = null;
	}
	
	@Test
	public void testAñadirPruebaNormal(){
		expect(prueba.getFecha()).andReturn(date3).once();
		expect(prueba.getPeso()).andReturn(0.1).anyTimes();
		replay(prueba);
		asignatura.añadirPrueba(prueba);
		assertTrue(asignatura.getPruebas().contains(prueba));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAñadirPruebaFechaInferior() {
		expect(prueba.getFecha()).andReturn(new Date(9)).once();
		expect(prueba.getPeso()).andReturn(0.1).anyTimes();
		replay(prueba);
		asignatura.añadirPrueba(prueba);
		verify();
		assertTrue(asignatura.getPruebas().contains(prueba));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAñadirPruebaFechaSuperior() {
		expect(prueba.getFecha()).andReturn(new Date(999)).once();
		expect(prueba.getPeso()).andReturn(0.1).anyTimes();
		replay(prueba);
		asignatura.añadirPrueba(prueba);
		verify();
		assertTrue(asignatura.getPruebas().contains(prueba));
	}
	
	public void testAñadirPruebaPonderacionSuperior(){
		expect(prueba.getFecha()).andReturn(new Date(9)).once();
		expect(prueba.getPeso()).andReturn(1.1).anyTimes();
		replay(prueba);
		asignatura.añadirPrueba(prueba);
		verify();
		assertTrue(asignatura.getPruebas().contains(prueba));
	}
	
	@Test
	public void testIsFechaPruebaEntreFechasAsignatura() {
		expect(prueba.getFecha()).andReturn(date3).once();
		replay(prueba);
		boolean valido 
				= asignatura.isFechaPruebaEntreFechasAsignatura(prueba);
		verify();
		
		assertTrue(valido);
	}
	
	public void testIsFechaPruebaFueraFechasAsignatura() {
		expect(prueba.getFecha()).andReturn(new Date(999)).once();
		replay(prueba);
		boolean valido 
				= asignatura.isFechaPruebaEntreFechasAsignatura(prueba);
		verify();
		
		assertFalse(valido);
	}
	
	@Test
	public void testGetSumaTotalPonderaciones() { 
		expect(prueba.getPeso()).andReturn(0.1).anyTimes();
		replay(prueba);
		 boolean valido = 
				 asignatura.isSumaPesosValido(prueba);
		verify();	
		assertTrue(valido);
	}

	@Test
	public void testGetSumaTotalFueraPonderaciones() {
		expect(prueba.getPeso()).andReturn(1.1).anyTimes();
		replay(prueba);
		 boolean valido = 
				 asignatura.isSumaPesosValido(prueba);
		verify();	
		assertFalse(valido);
	}
	
	@Test
	public void testTodasLasPruebasCalificadas() {
		expect(prueba.getFecha()).andReturn(date3).anyTimes();
		expect(prueba1.getFecha()).andReturn(date3).anyTimes();
		expect(prueba.isCompletamenteCalificada()).andReturn(true).anyTimes();
		expect(prueba1.isCompletamenteCalificada()).andReturn(true).anyTimes();
		expect(prueba.getPeso()).andReturn(0.5).anyTimes();
		expect(prueba1.getPeso()).andReturn(0.5).anyTimes();
		replay(prueba);
		replay(prueba1);
		
		asignatura.añadirPrueba(prueba1);
		asignatura.añadirPrueba(prueba);

		
		boolean calificadasTodas = 
				asignatura.todasLasPruebasCalificadas();
		verify();
		assertTrue(calificadasTodas);
	}
	
	@Test
	public void testTodasLasPruebasCalificadasNo() {
		expect(prueba.getFecha()).andReturn(date3).anyTimes();
		expect(prueba1.getFecha()).andReturn(date3).anyTimes();
		expect(prueba.isCompletamenteCalificada()).andReturn(true).anyTimes();
		expect(prueba1.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba.getPeso()).andReturn(0.5).anyTimes();
		expect(prueba1.getPeso()).andReturn(0.5).anyTimes();
		replay(prueba);
		replay(prueba1);
		
		asignatura.añadirPrueba(prueba1);
		asignatura.añadirPrueba(prueba);

		
		boolean calificadasTodas = 
				asignatura.todasLasPruebasCalificadas();
		verify();
		assertFalse(calificadasTodas);
	}
	
	@Test(expected = IllegalStateException.class)
	public void obtenerListadoCalificacionesFinalesNoCalificadas(){
		expect(prueba.getFecha()).andReturn(date3).anyTimes();
		expect(prueba1.getFecha()).andReturn(date3).anyTimes();
		expect(prueba.isCompletamenteCalificada()).andReturn(true).anyTimes();
		expect(prueba1.isCompletamenteCalificada()).andReturn(false).anyTimes();
		expect(prueba.getPeso()).andReturn(0.5).anyTimes();
		expect(prueba1.getPeso()).andReturn(0.5).anyTimes();
		replay(prueba);
		replay(prueba1);
		
		asignatura.añadirPrueba(prueba1);
		asignatura.añadirPrueba(prueba);
		
		asignatura.obtenerListadoCalificacionesFinales();
		verify();
	}
}
