package com.pabcarr;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Date;
import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;

/**
 * Prueba los métodos y funciones de la clase Asignatura
 * 
 * @author pabcarr
 *
 */
public class AsignaturaFuncionesTest {

	private Asignatura asignatura;
	private IPrueba prueba;
	private IPrueba prueba1;
	private IPrueba prueba2;
	private Hashtable<String, Double> calificaciones;
	
	@Before
	public void setUp() {
		asignatura  = new Asignatura("TDS", "muy bonita", 10.0, new Date(199), new Date(230));
		prueba = new Prueba(new Date (205), "p1", "fasilita", 10.0, 0.1);
		prueba1 = new Prueba(new Date(304), "p2", "mediana", 10.0, 0.3);
		prueba2 = new Prueba(new Date(206), "p2", "complicada", 10.0, 1.0);
		
		calificaciones = new Hashtable<String, Double>();
		calificaciones.put("pepe", 8.0);
	}
	
	@After
	public void tearDown() {
		asignatura = null;
		prueba = null;
		prueba1 = null;
		prueba2 = null;
		calificaciones = null;
	}
	
	@Test
	public void testAñadirPruebaCorrecta() {
		asignatura.añadirPrueba(prueba);
		assertTrue(asignatura.getPruebas().contains(prueba));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAñadirPruebaFechaIncorrecta() {
		asignatura.añadirPrueba(prueba1);
	}
		
	@Test(expected = IllegalArgumentException.class)
	public void testAñadirPruebaSePasaDePonderacion() {
		asignatura.añadirPrueba(prueba);
		asignatura.añadirPrueba(prueba2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testComprobarPesoSePasaPasandose() {
		asignatura.añadirPrueba(prueba);
		asignatura.añadirPrueba(prueba2);
	}
	
	@Test
	public void testFechaEntreAsignaturaSi() {
		boolean correcto = 
				asignatura.isFechaPruebaEntreFechasAsignatura(prueba);
		
		assertEquals(correcto, true);
	}
	
	@Test
	public void testFechaEntreAsignaturaNo(){
		boolean correcto =
				asignatura.isFechaPruebaEntreFechasAsignatura(prueba2);
		assertEquals(correcto, false);
	}
		
	
	@Test(expected = IllegalArgumentException.class)
	public void testAñadirPruebaNull() {
		asignatura.añadirPrueba(null);
	}
	
	@Test
	public void testTodasPruebasCalificadasSi() {
		prueba.marcarCalificada(new Date(207));
		asignatura.añadirPrueba(prueba);
		
		boolean calificada =
				asignatura.todasLasPruebasCalificadas();
		assertEquals(calificada, true);
	}
	
	@Test
	public void testTodasPruebasCalificadasNo() {
		asignatura.añadirPrueba(prueba);
		boolean calificada = 
				asignatura.todasLasPruebasCalificadas();
		
		assertEquals(calificada, false);
	}
	
	@Test
	public void testObtenerListadoCalificacionesFinalesCorrecto() {
		prueba2.calificar(calificaciones, new Date(206));
		asignatura.añadirPrueba(prueba2);
		
		Hashtable<String, Double> calificaciones = 
				asignatura.obtenerListadoCalificacionesFinales();
		
		assertEquals(calificaciones.size(), 1);
		assertEquals(calificaciones.get("pepe").doubleValue(), 8.0, 0.001);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testObtenerListadoCalificacionesFinalesSumaNoUno() {
		prueba.calificar(calificaciones, new Date(206));
		asignatura.añadirPrueba(prueba);
		 
		asignatura.obtenerListadoCalificacionesFinales();
	}
	
	@Test(expected = IllegalStateException.class)
	public void testObtenerListadoCalificacionesFinalesNoCalificadas() {
		asignatura.añadirPrueba(prueba2);
		asignatura.obtenerListadoCalificacionesFinales();
	}
	
	@Test
	public void testObtenerListadoCalificacionesParciales() {
		prueba.calificar(calificaciones, new Date(206));
		asignatura.añadirPrueba(prueba);
		
		Hashtable<String, Double> calificaciones =
				asignatura.obtenerListadoCalificacionesParciales();
		
		assertEquals(calificaciones.size(), 1);
		assertEquals(calificaciones.get("pepe").doubleValue(), 0.8, 0.001);
	}
}
