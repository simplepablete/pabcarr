package com.pabcarr;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	AtributosAsignaturaTest.class,
	AsignaturaFuncionesTest.class
})
public class AsignaturaTDDTests {

}
