package com.pabcarr;
import java.util.Date;
import java.util.Hashtable;
/**
 * Prueba de una asignatura. Puede calificarse.
 * Tiene tanto un nombre, una breve descripción, como una fecha de
realización.
 * @author anonymous
 */
public class Prueba implements IPrueba {
      protected Hashtable<String, Double> calificaciones;
     
      /**
       * Inicializa una prueba.
       * @param fecha Fecha de realización de la prueba
       * @param nombre Identificador de la prueba
       * @param descripcion Breve información acerca de la prueba
       * @param notaMax nota máxima que puede obtenerse en la prueba
       * @param peso  Porcentaje (valor entre 0 y 1) que la prueba tiene en una asignatura
       */
      public Prueba(Date fecha, String nombre, String descripcion, double 
    		  notaMax, double peso){
            // TODO
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#isCompletamenteCalificada()
	 */
      public boolean isCompletamenteCalificada(){
            // TODO cambiar, implementación falsa
            return true;
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getCalificaciones()
	 */
      public Hashtable<String, Double> getCalificaciones(){
            // TODO cambiar, implementación falsa
            return calificaciones;
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#marcarCalificada(java.util.Date)
	 */
      
      public void marcarCalificada(Date fechaActual){
            // TODO
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#calificar(java.lang.String, double, java.util.Date)
	 */
		public void calificar(String id,double nota, Date fechaActual){
		// TODO 
		}
	
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#calificar(java.util.Hashtable, java.util.Date)
	 */
      public void calificar(Hashtable<String, Double> calificaciones, Date fechaActual){
    	  // TODO 
      }
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#modificar(java.lang.String, double)
	 */
      public void modificar(String id, double nota){
            // TODO
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getPeso()
	 */
      public double getPeso(){
            // TODO cambiar, implementación falsa
            return 0;
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getNombre()
	 */
      public String getNombre() {
            // TODO cambiar, implementación falsa
            return "";
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getDescripcion()
	 */
      public String getDescripcion() {
            // TODO cambiar, implementación falsa
            return "";
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getNotaMax()
	 */
      public double getNotaMax() {
            // TODO cambiar, implementación falsa
            return 0.1;
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getFecha()
	 */
      public Date getFecha() {
            // TODO cambiar, implementación falsa
            return new Date(206);
      }
      
      /* (non-Javadoc)
	 * @see es.uva.inf.tds.entornoeducativo.IPrueba#getNota(java.lang.String)
	 */
      public double getNota(String alumno){
            // TODO cambiar, implementación falsa
            return 0;
      } 
}