package com.pabcarr;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Set;

public class Asignatura implements IAsignatura{

	private String nombre;
	private String descripcion;
	private double calificacionMaxima;
	private Date fechaInicio;
	private Date fechaFin;
	private ArrayList<IPrueba> pruebas;
	
	/**
	 * Constructor vacio, usado para los tests.
	 */
	protected Asignatura(){
		pruebas = new ArrayList<IPrueba>();
	}
	
	/**
	 * Constructor de la clase Asignatura
	 * @param nombre nombre de la asignatura
	 * @param descripcion descripcion de la asignatura
	 * @param calificacionMaxima calificacion maxima de la asignatura
	 * @param fechaInicio fecha de comienzo de la asignatura
	 * @param fechaFin fecha de final de la asignatura
	 * @throws IllegalArgumentException si el nombre es null
	 * @throws IllegalArgumentException si el nombre es la cadena vacia ""
	 * @throws IllegalArgumentException si la descripcion es la cadena vacia
	 * @throws IllegalArgumentException si la descripcion es null
	 * @throws IllegalArgumentException si la calificacion maxima es negativa
	 * @throws IllegalArgumentException si la calificacion maxima es cero
	 * @throws IllegalArgumentException si la fecha de inicio es null
	 * @throws IllegalArgumentException si el parametro es null
	 * @throws IllegalArgumentException si la el parametro es anterior que la fecha de inicio
	 */
	public Asignatura (String nombre, String descripcion, 
			double calificacionMaxima, Date fechaInicio, Date fechaFin) 
					throws IllegalArgumentException, IllegalStateException{	
		
		setNombre(nombre);
		setDescripcion(descripcion);
		setCalificacionMaxima(calificacionMaxima);
		setFechaInicio(fechaInicio);
		setFechaFin(fechaFin);
		this.pruebas = new ArrayList<IPrueba>();
	}
	
	/**
	 * Inserta como nombre de la asignatura el parámetro
	 * @param nombreAsignatura
	 * @throws IllegalArgumentException si el nombre es null
	 * @throws IllegalArgumentException si el nombre es la cadena vacia ""
	 */
	protected void setNombre(String nombreAsignatura) throws IllegalArgumentException {
		
		if(nombreAsignatura == null) {
			throw new IllegalArgumentException();
		}
		
		if(nombreAsignatura.equals("")) {
			throw new IllegalArgumentException();
		} 
		
		this.nombre = nombreAsignatura;
	}
	
	/**
	 * Inserta en la descripcion de la asignatura el parametro
	 * @param descripcion descripcion de la asignatura
	 * @throws IllegalArgumentException si la descripcion es la cadena vacia
	 * @throws IllegalArgumentException si la descripcion es null
	 */
	protected void setDescripcion(String descripcion) throws IllegalArgumentException {
		
		if(descripcion == null) {
			throw new IllegalArgumentException();
		}
		
		if(descripcion.equals("")) {
			throw new IllegalArgumentException();
		} 
		this.descripcion = descripcion;
	}
	
	/**
	 * Inserta en la calificacion maxima el parametro
	 * @param calificacion calificacion maxima de la asignatura
	 * @throws IllegalArgumentException si la calificacion maxima es negativa
	 * @throws IllegalArgumentException si la calificacion maxima es cero
	 */
	protected void setCalificacionMaxima(double calificacion) 
			throws IllegalArgumentException {	
		
		if(calificacion <=0) {
			throw new IllegalArgumentException();
		} 
		this.calificacionMaxima = calificacion;
	}
	
	/**
	 * Inserta como fecha de inicio el parametro
	 * @param fechaInicio fecha de inicio de la asignatura
	 * @throws IllegalArgumentException si la fecha es null
	 */
	protected void setFechaInicio(Date fechaInicio) 
			throws IllegalArgumentException {
		
		if(fechaInicio == null) {
			throw new IllegalArgumentException();
		}
		this.fechaInicio = fechaInicio;
	}
	
	/**
	 * Inserta como fecha fin el parámetro de la asignatura
	 * @param fechaFin parametro de la asignatura
	 * @throws IllegalArgumentException si el parametro es null
	 * @throws IllegalArgumentException si el parametro es anterior que la fecha de inicio
	 * @throws IllegalArgumentException si el parametro es igual a la de inicio 
	 */
	protected void setFechaFin(Date fechaFin) 
			throws IllegalArgumentException {
		
		if(fechaFin == null) {
			throw new IllegalArgumentException();
		}
		if(fechaFin.before(this.fechaInicio)){
			throw new IllegalArgumentException();
		}
		
		if(fechaFin.equals(fechaInicio)){
			throw new IllegalArgumentException();
		}
		
		this.fechaFin = fechaFin;
	}
	
	/**
	 * Comprueba si el peso de añadir esa prueba es mayor que 1
	 * @param prueba prueba a introducir
	 */

	protected boolean isSumaPesosValido( IPrueba prueba){		
		double peso = getSumaTotalPonderaciones();
		if(peso + prueba.getPeso() >1){
			return false;
		}
		return true;
	}
	
	/**
	 * Si la fecha de una prueba a comprobar esta entre las fechas de la asignatura.
	 * @param prueba prueba a comprobar
	 * @return true si esta dentro de las fechas, false en caso contrario
	 */
	protected boolean isFechaPruebaEntreFechasAsignatura(IPrueba prueba) {
		Date fechaDeLaPrueba = prueba.getFecha();
		boolean fechaValida = fechaDeLaPrueba.after(fechaInicio) &
							fechaDeLaPrueba.before(fechaFin);
		if(fechaValida){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Comprueba si todas las pruebas han sido calificadas
	 * @return true si han sido todas calificadas, false en caso contrario
	 */
	protected boolean todasLasPruebasCalificadas(){
		for (IPrueba p: pruebas) {
			if(!p.isCompletamenteCalificada()){
				return false;
			}
		}
		return true;
	}

	private double getSumaTotalPonderaciones(){
		double peso = 0;
		for(IPrueba p: pruebas){
			peso = peso + p.getPeso();
		}
		return peso;
	} 
	
	public String getNombre() {
		return this.nombre;
	}

	public String getDescripción() {
		return this.descripcion;
	}

	public double getCalificacionMaxima() {
		return this.calificacionMaxima;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void añadirPrueba(IPrueba pruebaAIntroducir) 
			throws IllegalArgumentException{
		
		if( pruebaAIntroducir == null) throw new IllegalArgumentException();
		boolean valida = 
				isFechaPruebaEntreFechasAsignatura(pruebaAIntroducir) &
				isSumaPesosValido(pruebaAIntroducir);
		if(valida){
			pruebas.add(pruebaAIntroducir);
		} else {
			throw new IllegalArgumentException();
		}
				
	}
	
	public Hashtable<String, Double> obtenerListadoCalificacionesFinales() 
			throws IllegalStateException {
		
		if (!todasLasPruebasCalificadas()) {
			throw new IllegalStateException();
		}
		
		if(getSumaTotalPonderaciones() > 1.0) {
			throw new IllegalStateException();
		}
		
		Hashtable<String, Double> calificacionesFinales 
											= new Hashtable<String, Double>();
		
		for(IPrueba p : pruebas) {
			double ponderacion = p.getPeso();
			Hashtable<String, Double> calificacionesTmp;
			calificacionesTmp = p.getCalificaciones();
			if(calificacionesTmp != null){
				Set<String> claves = calificacionesTmp.keySet();
				for(String s: claves) {
					double valor = 
							calificacionesTmp.get(s).doubleValue() * ponderacion;
					valor = valor + calificacionesFinales.get(s).doubleValue();
					calificacionesFinales.put(s, valor);
				}
			}
			
		}
		return calificacionesFinales;
	}

	public Hashtable<String, Double> obtenerListadoCalificacionesParciales() {
		
		Hashtable<String, Double> calificacionesFinales 
										= new Hashtable<String, Double>();

		for(IPrueba p : pruebas) {
			if( p.isCompletamenteCalificada() ){
				double ponderacion = p.getPeso();
				Hashtable<String, Double> calificacionesTmp;
				calificacionesTmp = p.getCalificaciones();
				
				if(calificacionesTmp != null){
					Set<String> claves = calificacionesTmp.keySet();
					for(String s: claves) {
						double valor = 
								calificacionesTmp.get(s).doubleValue() * ponderacion;
						valor = valor + calificacionesFinales.get(s).doubleValue();
						calificacionesFinales.put(s, valor);
					}
				}
			}
		}
			return calificacionesFinales;
	}

	public ArrayList<IPrueba> getPruebas() {
		return pruebas ;
	}
}
