package com.pabcarr;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
/**
 * 
 * @author pabcarr
 *
 */
public interface IAsignatura {

	/**
	 * Devuelve el nombre de la asignatura.
	 * @return nombre de la Asignatura
	 */
	String getNombre();
	
	/**
	 * Devuelve la descripción de la asignatura
	 * @return la descripción de la asignatura
	 */
	String getDescripción();
	
	/**
	 * Devuelve la calificación máxima posible en la asignatura
	 * @return la descripcion máximia posible en la asignatura
	 */
	double getCalificacionMaxima();
	
	/**
	 * Devuelve la fecha de inicio de la asignatura
	 * @return la fecha de inicio de la asignatura
	 */
	Date getFechaInicio();
	
	/**
	 * Devuelve la fecha de fin de la asignatura
	 * @return la fecha de fin de la asignatura
	 */
	Date getFechaFin();
	
	
	/**
	 * Añade una prueba a una asignatura
	 * @param asignaturaAIntroducir
	 * @throws IllegalArgumentException si la prueba es Null
	 * @throws IllegalArgumentException si la fecha de la prueba no esta entre las de la asignatura.
	 * @throws IllegalArgumentException si la ponderacion de la prueba 
	 * sumada a la actual sobrepasa 1
	 */
	void añadirPrueba(IPrueba pruebaAIntroducir) throws IllegalArgumentException;
	
	
	/**
	 * Devuelve todas las pruebas de la asignatura
	 * @return todas las pruebas de la asignatura. null si no hay.
	 */
	ArrayList<IPrueba> getPruebas();
	
	/**
	 * Calificaciones finales de la asignatura. 
	 * @throws IllegalStateException si la suma de las ponderaciones de las 
	 * pruebas no es 1
	 * @throws IllegalStateException si la suma de las ponderaciones de las 
	 * pruebas es 1 y no estan todas marcadas como calificadas
	 */
	Hashtable<String, Double> obtenerListadoCalificacionesFinales() throws IllegalStateException;
	
	/**
	 * Calificaciones parciales de la asignatura. 
	 * Suma ponderada de las pruebas que están marcadas como finalizadas. 
	 */
	Hashtable<String, Double> obtenerListadoCalificacionesParciales();
}
