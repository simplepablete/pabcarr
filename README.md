#Practica Final TDS

Este repositorio da respuesta a la práctica 4 (Final) de la asignatura Tecnologias para el Desarrollo del Software.

##Autores: 

* Pablo Carrascal

### Obtención del repositorio:

```
git clone https://simplepablete@bitbucket.org/simplepablete/pabcarr.git
cd pabcarr
```

### Ejecución:

Los distintos objetivos de la práctica pueden conseguirse tanto importando el proyecto en eclipse y ejecutando el fichero Ant o mediante la ejecución en shell de los objetivos Ant correspondientes

### Tareas:

- Compilar 

```
ant compilar
```

- Ejecutar **todos los tests**

```
ant ejecutarTodo
```

- Ejecutar los **test de tdd**

```
ant ejecutarTestTDD
```

- Ejecutar los **test en aislamiento**

```
ant ejecutarTestEnAislamiento
```

- Obtener los informes de cobertura de los test.

```
ant obtenerInformeCobertura 
```

- Ejecutar pruebas de secuencia

```
ant ejecutarPruebasSecuencia
```

Los informes generados pueden ser accedidos desde `/target/site/jacoco/index.html` 


- Generar el Javadoc

```
ant javaDoc
```

## Dependencias

Las dependencias internas se gestionan mediante **Maven**

El resto son:

* [Java JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/)
* [Ant](http://ant.apache.org)
* [Maven](http://maven.apache.org)